#!
#    ___              _      _               ___     ___   
#   | __|     _ _    (_)    | |__    ___    / _ \   / __|  
#   | _|     | '_|   | |    | / /   |___|  | (_) |  \__ \  
#   |___|   _|_|_   _|_|_   |_\_\   _____   \___/   |___/  
# _|"""""|_|"""""|_|"""""|_|"""""|_|     |_|"""""|_|"""""| 
# "`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-' 
#
# NAME: Erik-OS Windows 11
# DESC: An installation and deployment script for erik's win11 desktop
# Concept based on https://gitlab.com/dwt1/dtos/-/blob/master/dtos
# Also based on https://github.com/farag2/Sophia-Script-for-Windows

Clear-Host
$Host.UI.RawUI.WindowTitle = " Script for Erik-OS Win11 | Made with $([char]::ConvertFromUtf32(0x1F497)) of Windows"
Write-Output "Setting up Win11 for me..."

# https://gist.github.com/crutkas/6c2096eae387e544bd05cde246f23901

Write-Output "#####################"
Write-Output "## Installing Apps ##"
Write-Output "#####################"

$apps = @(
    @{name = "Microsoft.PowerShell" }, 
    @{name = "Microsoft.VisualStudioCode" }, 
    @{name = "Microsoft.WindowsTerminal" }, 
    @{name = "Microsoft.PowerToys" },
    @{name = "WinSCP.WinSCP" },
    @{name = "Mozilla.Firefox" },
    @{name = "7zip.7zip" },
    @{name = "KeePassXCTeam.KeePassXC" },
    @{name = "Discord.Discord" },
    @{name = "Valve.Steam" },
    @{name = "Neovim.Neovim" },
    @{name = "Telegram.TelegramDesktop" },
    @{name = "Git.Git" },
    @{name = "Inkscape.Inkscape" },
    @{name = "GIMP.GIMP" }
);

Foreach ($app in $apps) {
    $listApp = winget list --exact -q $app.name --accept-source-agreements 
    if (![String]::Join("", $listApp).Contains($app.name)) {
        Write-host "Installing:" $app.name
        if ($app.source -ne $null) {
            winget install --exact --silent $app.name --source $app.source --accept-package-agreements --accept-source-agreements 
        }
        else {
            winget install --exact --silent $app.name --accept-package-agreements --accept-source-agreements
        }
    }
    else {
        Write-host "Skipping Install of " $app.name
    }
}

# Reloading path
$env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path","User")

Write-Output "##############################"
Write-Output "## Installing VSCodePlugins ##"
Write-Output "##############################"

code --install-extension ms-python.python
code --install-extension ms-python.vscode-pylance
code --install-extension KevinRose.vsc-python-indent
code --install-extension njpwerner.autodocstring
code --install-extension dracula-theme.theme-dracula
code --install-extension eamodio.gitlens
code --install-extension vscodevim.vim

Write-Output "#########################"
Write-Output "## Installing Dev Font ##"
Write-Output "#########################"

Set-Location $HOME\Downloads
git clone --filter=blob:none --sparse https://github.com/ryanoasis/nerd-fonts
Set-Location nerd-fonts
git sparse-checkout add patched-fonts/FiraCode
./install.ps1 FiraCode
Set-Location $HOME\Downloads
Remove-Item -Recurse -Force nerd-fonts
Set-Location $HOME

Write-Output "#################################"
Write-Output "## Customizations and Dotfiles ##"
Write-Output "#################################"

Install-Module posh-git -Scope CurrentUser -Force
Install-Module oh-my-posh -Scope CurrentUser -Force
Install-Module Terminal-Icons -Scope CurrentUser -Repository PSGallery -Force
Install-Module -Name PSReadLine -AllowPrerelease -Scope CurrentUser -Repository PSGallery -Force -SkipPublisherCheck
Install-Module PSFzf -Scope CurrentUser -Force

git clone 

# Updating user profile to point at config
$profiletext = '. $env:USERPROFILE\.config\powershell\user_profile.ps1'
$profiletext | Out-File $PROFILE

Write-Output "#########################"
Write-Output "## Cleaning up Windows ##"
Write-Output "#########################"

$apps = "*3DPrint*",
        "Microsoft.MixedReality.Portal", 
        "Microsoft.Bing*",
        "Microsoft.GamingApp",
        "Microsoft.XboxApp", 
        "Microsoft.WindowsFeedbackHub",
        "Microsoft.549981C3F5F10",
        "Microsoft.MicrosoftStickyNotes",
        "Microsoft.MicrosoftSolitaireCollection",
        "Microsoft.WindowsSoundRecorder",
        "Microsoft.YourPhone",
        "Microsoft.ZuneMusic",
        "Microsoft.ZuneVideo",
        "microsoft.windowscommunicationsapps"

Foreach ($app in $apps)
{
    Write-host "Uninstalling:" $app
    Get-AppxPackage -allusers $app  | Remove-AppxPackage
}

# Now Telemetry
# Connected User Experiences and Telemetry
Get-Service -Name DiagTrack | Stop-Service -Force
Get-Service -Name DiagTrack | Set-Service -StartupType Disabled
# Block connection for the Unified Telemetry Client Outbound Traffic
Get-NetFirewallRule -Group DiagTrack | Set-NetFirewallRule -Enabled False -Action Block
